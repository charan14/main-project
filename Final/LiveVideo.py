#From live video stream
import ocr
import Text2Audio as TTS
import Process_Image as ps
import glob, os


# Delete all previous frames
for file in glob.glob("frames//*.jpg"):
    os.remove(file)

#Empty output files
for file in glob.glob("output//*.jpg"):
    os.remove(file)

os.system('python frames.py')
count = 0
for image in glob.glob("output//*.jpg"):
	count += 1

for image in glob.glob("output//*.jpg"):
	if count != 1:
		os.remove(image)
		count -= 1

# Apply pre-processing Techniques
# Input 2 parameters
# 1.Image Path
# 2.Technique(Default="thresh" other "blur")

#Apply Opening
image = ps.Image_Preprocess(image,preprocess="opening")


#For Direct Ocr [No preprocessing]
# image = ps.Image_Preprocess(image,preprocess="none")


# This method extracts the text in the image to raw text.
# Input 2 parameters
# 1. Image path
# 2. Language to detect
text = ocr.ImageToText(image,'eng')


print("=====================")
print("Detected Text is ")

print(text)
print("=====================")

# Convert the extracted text to Audio using "pyttsx3"
# Input: Raw text
TTS.Text2Audio(text)

