import pytesseract

def ImageToText(img,lang):
    print("[Message] Started Text Extraction.")
    pytesseract.pytesseract.tesseract_cmd = r'C:\\Program Files (x86)\\Tesseract-OCR\\tesseract.exe'
    text = pytesseract.image_to_string(img, lang=lang, config='--tessdata-dir "C:\\Program Files (x86)\\Tesseract-OCR\\tessdata"')
    print("[Message] Text Extraction done.")
    return(text)
