import ocr
import text_detection
import TextToSpeech.Text2Audio as TTS
import cv2, glob
import numpy as np

list = text_detection.DetectText('images/48.jpg','frozen_east_text_detection.pb')
print(list)
img = cv2.imread(list, 0)
imga = img
#Opening
kernel = np.ones((3,2), np.uint8)
img = cv2.morphologyEx(img,cv2.MORPH_OPEN, kernel)
cv2.imshow('Input Image', imga)

cv2.imwrite("output/out.jpg", img)
cv2.waitKey(0)
text = ''

text = ocr.ImageToText("output/out.jpg",'eng')

print("Detected Text:")
print(text)
TTS.Text2Audio(text)
