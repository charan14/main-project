import numpy as np
import cv2
import time, os, glob
import text_detection as td


# The duration in seconds of the video captured
capture_duration = 1

# cap = cv2.VideoCapture("http://192.168.43.2:8080/video")

cap = cv2.VideoCapture(0)
start_time = time.time()
count = 0
while( int(time.time() - start_time) < capture_duration ):
    ret, frame = cap.read()
    if ret == True:
        name = "frames/" + str(count) + ".jpg"
        cv2.imwrite(name, frame)
        count += 1
    else:
        break
print("Frames Captured")
count = 0
for file in glob.glob("frames/*.jpg"):
    frame = td.DetectText(file,'../Project/frozen_east_text_detection.pb')

    if frame[0] != 0:
        print("Frame " + file + " Deleted")
        os.remove(file)
    else:
        gray = cv2.cvtColor(frame[1], cv2.COLOR_BGR2GRAY)
        blur = int(round(cv2.Laplacian(gray, cv2.CV_16S, ksize=1).var()))
        dst = "frames/"+str(blur)+".jpg"
        try:
            os.rename(file, dst)
            print("Frame "+dst+" Saved")
        except:
            os.remove(file)
        count += 1

print("[Message] Text Detected In "+str(count)+" Frames.")