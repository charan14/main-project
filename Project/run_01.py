import ocr
import text_detection
import Text2Audio as TTS
import Process_Image as ps
from PIL import Image
import PIL.ImageOps
import cv2
import glob, os

for file in glob.glob("images\\test\\*.jpg"):

    # Image Path
    image = file

    # Apply pre-processing Techniques
    # Input 2 parameters
    # 1.Image Path
    # 2.Technique(Default="thresh" other "blur")
    image = ps.Image_Preprocess(image)

    # image = "E:\\Main Project\\Python Project\\Project\\images\\test\\"+image

    #
    # # This method is to crop the detected words in the image using EAST
    # # Input 5 parameters
    # # 1.Image Path
    # # 2.Model file path
    # # 3.Minimum Confidence(Default 0.5)
    # # 4 & 5 Width and Height (Default 320)
    # # Returns list of images path (Detected words as Images)
    #
    list = text_detection.DetectText(image,'E:\\Main Project\\Python Project\\Project\\frozen_east_text_detection.pb')

    print(list)
    text = ''
    for img in list:
        img = ps.Image_Preprocess(img)

        # This method extracts the text in the image to raw text.
        # Input 2 parameters
        # 1. Image path
        # 2. Language to detect
        text += ocr.ImageToText(img,'eng')+" "
    #
    # print("Detected Text for "+file)
    # text = ocr.ImageToText(image,'eng')
    print("=====================")
    print("text in "+file)
    print(text)

    # Convert the extracted text to Audio using "pyttsx3"
    # Input: Raw text
    TTS.Text2Audio(text)
    print("=====================")


