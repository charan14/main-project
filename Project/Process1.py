import ocr
import text_detection
import Text2Audio as TTS
import Process_Image as ps
import re

#Taking image from Images Folder
image = 'images/111.jpg' 

#Giving taken image as input to East and croping the text detected in the image
lista = text_detection.DetectText(image,'frozen_east_text_detection.pb')
text = ''

for img in lista:

    #Converting each image to gray scale
    img = ps.Image_Preprocess(img)

    #Giving Grayscale images to OCR
    txt = ocr.ImageToText(img,'eng')

    #Appending text in each image to String
    text += txt+" "

#Removing the unnecessary characters from detected text
text = re.sub(r'[?$.!-/_]',r'',text)
text = re.sub(r'[^a-zA-Z0-9 ]',r'',text)

#Printing Detected Text
print("Detected Text : ")
print(text.encode("utf-8"))
print("===================")

#Giving the detected text as input for TTS
TTS.Text2Audio(text)
