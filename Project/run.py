import ocr
import text_detection
import TextToSpeech.Text2Audio as TTS
import cv2, glob
import numpy as np

#Giving image as input to East and croping the text detected in the image
list = text_detection.DetectText('images/111.jpg','frozen_east_text_detection.pb')


#Applying opening for Croped image
kernel = np.ones((3,2), np.uint8)
img = cv2.morphologyEx(list,cv2.MORPH_OPEN, kernel)

#Display image
cv2.imshow('Opening', img)

#Save image to output folder
cv2.imwrite("output/out.jpg", img)
cv2.waitKey(0)
text = ''

#giving saved image as input to OCR
text = ocr.ImageToText("output/out.jpg",'eng')

#printing text
print("Detected Text:")
try:
    print(text)
except:
    print(text.encode("utf-8"))

#Giving detected text to TTS
TTS.Text2Audio(text)
