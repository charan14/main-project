import pytesseract

def ImageToText(img,lang):
    pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files (x86)\Tesseract-OCR\tesseract.exe'
    text = pytesseract.image_to_string(img, lang=lang, config='--tessdata-dir "C:\\Program Files (x86)\\Tesseract-OCR\\tessdata"')
    return(text)
