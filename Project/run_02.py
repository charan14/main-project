#to run multiple files at a time
import ocr
import text_detection
import Text2Audio as TTS
import Process_Image as ps
import KeyFrameExtraction as kf
from PIL import Image
import PIL.ImageOps
import cv2
import glob, os

#Delete all previous frames
for file in glob.glob("images//frames//*.jpg"):
    os.remove(file)

#Empty output files
for file in glob.glob("output//*.jpg"):
    os.remove(file)

file = 'videos/video_01.mp4'
dir = 'images/frames/'
len_window = 10

image = kf.KFE(file,dir,len_window)
# print(image)

# Apply pre-processing Techniques
# Input 2 parameters
# 1.Image Path
# 2.Technique(Default="thresh" other "blur")
image = ps.Image_Preprocess(image)

# This method extracts the text in the image to raw text.
# Input 2 parameters
# 1. Image path
# 2. Language to detect
text = ocr.ImageToText(image,'eng')
print("=====================")
print("Detected Text is ")
print(text)

# Convert the extracted text to Audio using "pyttsx3"
# Input: Raw text
TTS.Text2Audio(text)
print("=====================")
