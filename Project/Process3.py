import ocr
import text_detection
import Text2Audio as TTS
import re

#Taking image from Images Folder
image = 'images/111.jpg'

#Giving taken image as input to East and croping the text detected in the image
lista = text_detection.DetectText(image,'frozen_east_text_detection.pb')
text = ''

for img in lista:
    #Giving the croped images from EAST to OCR
    txt = ocr.ImageToText(img,'eng')

    #Appending detected text
    text += txt+" "

#Removing the unnecessary characters from detected text
text = re.sub(r'[?$.!-/_]',r'',text)
text = re.sub(r'[^a-zA-Z0-9 ]',r'',text)

#Printing Detected Text
print("Detected Text : ")
print(text.encode("utf-8"))
print("===================")

#Giving the detected text as input for TTS
TTS.Text2Audio(text)
