import cv2
import os
import numpy as np

def Image_Preprocess(img,preprocess):
    # load the example image and convert it to grayscale

    print("[Message] Started Preprossing for Extracted Frame.")
    image = cv2.imread(img)

    if preprocess == "thresh":
        out = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        out = cv2.threshold(out, 0, 255,cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

    elif preprocess == "blur":
        out = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        out = cv2.medianBlur(out, 3)
        blur = cv2.GaussianBlur(out, (5, 5), 0)
        out = cv2.addWeighted(blur, 1.5, out, -0.5, 0)

    elif preprocess == "opening":
        kernel = np.ones((3, 2), np.uint8)
        out = cv2.morphologyEx(image, cv2.MORPH_OPEN, kernel)

    else:
        out = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # write the grayscale image to disk as a temporary file so we can
    # apply OCR to it
    filename = "output//{}.jpg".format(os.getpid())
    cv2.imwrite(filename, out)
    print("[Message] Preprossing phase done.")
    return(filename)