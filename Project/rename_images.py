import os


# Function to rename multiple files
def main():
    i = 100

    for filename in os.listdir("GMRIT/"):
        dst = str(i) + ".jpg"
        src = 'GMRIT/' + filename
        dst = 'GMRIT/' + dst

        # rename() function will
        # rename all the files
        os.rename(src, dst)
        i += 1


# Driver Code
if __name__ == '__main__':
    # Calling main() function
    main()